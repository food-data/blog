---
author: Onno Haldar
title: Paco Ciao
date: 2023-11-03
description: Een trendy lunch met verassende entree
categories: [restaurant, bistro]
tags: [lunch, vis, vega]
---

[Paco Ciao](https://www.pacociao.nl) is vlak bij het Centraal Station van Leiden gelegen. Aanrader als je als (dag)toerist van een vis of vega-lunch wil genieten. Zeer trendy (hipster-like) ingericht met sfeervolle oude soul- en regeamuziek.
Gerechten zijn salades met Sushi- en/of Vega-achtige Vibes. Zeer smakelijk en vers.
NB: Er is ook een grote tafel in de serre die we nog een keer willen boeken 