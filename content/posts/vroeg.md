---
author: Onno Haldar
title: Vroeg Restaurant
date: 2024-03-31
description: Een Paas High Thea
categories: [restaurant, lunchroom]
tags: [lunch, brood]
---

[Vroeg Restaurant](https://www.vroeg.nl) heeft met Paasen en leuke aanbieding voor een Paas High Thea. Daar hebben we met het gezin op 1e Paasdag dankbaar gebruik van gemaakt. Was prima met veel vers gebakken broodjes uit eigen bakkerij.