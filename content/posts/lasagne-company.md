---
author: Onno Haldar
title: Lasagne Company
date: 2024-03-30
description: Een Lunch in Oud-Delft
categories: [brasserie, restaurant, italiaanse keuken]
tags: [lunch, pasta]
---

De [Lasagne Company](https://lasagnecompany.com) is een gezellig restaurant / lunchroom in het historische centrum van Delft. De eigenaar heeft vanuit zijn passie geinevesteerd in de smaak van de Lasagne via een selectie van kruiden en ingredienten. Dat hebben ondervonden als zeer smakelijke ervaring met heerlijke Italiaanse drank [Cannela - Bellini](../../essays/canella-bellini/) als leuke extra ontdekking.

