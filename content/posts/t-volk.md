---
author: Onno Haldar
title: Restaurant 't Volk
date: 2023-06-23
description: Diner met Oosterse Tapas in Arnhem
categories: [restaurant, oosterse keuken]
tags: [diner, tapas]
---

[Restaurant 't Volk](https://volkarnhem.nl) in Arnhem biedt een kaart met onbeperkte Oosterse Tapas. Het is midden in Arnhem gelegen in de volksbuurt en je kan lekker buiten zitten. Wij waren daar op een zomerse dag en hebben zowel genoeten van de heerlijke en orginele gerechten, als de omgeving waar genoeg vermaak op straat voorbioj trok ;-)