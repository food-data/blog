---
author: Onno Haldar
title: Tabú
date: 2024-06-02
description: Een Latijns-Amerikaanse Brunch met Cocktails
categories: [restaurant, latijns-amerikaanse keuken]
tags: [lunch, cocktails]
---

[Tabú](https://www.tabu.nl) is een sfeervol gelegen aan de Witte Singel in Leiden. Aanrader is de maandelijkse op zondag te reserveren `Bottomless Taboozy Brunch` waarbij je onbeperkt cocktails kan drinken voor 1 vast prijs. Daarbij worden meerdere warme en koude gerechten geserveerd.