---
author: Onno Haldar
title: Restaurant Rodaen
date: 2024-03-22
description: Een lunch met diverse Tapas
categories: [restaurant, bistro]
tags: [lunch, tapas]
---

[Restaurant Rodaen](https://www.rodaen.nl) is een plaats waar je gezellig en smakelijk kan lunchen. Wij hebben daar mijn verjaardag gevierd met een lunch op basis van onbeperkte Tapas-gerechten. Dit is zeer goed bevallen met zeer smakelijke en soms verassende gerechten zoals de Bloemkool-schotel.