---
author: Onno Haldar
title: Cardi's Café Brasserie
date: 2024-11-17
description: Zondag-lunch aan Zee
categories: [brasserie, lunchroom, cafe]
tags: [lunch, belgisch]
---

[Cardi's Café Brasserie](https://cardiscafe.be) kwamen we toevalloig tegen terwijl we op zoek waren naar een lunchgelegenheid. Bleek lokaal bekend te staan om het [Zondag lunch menu](https://cardiscafe.be/menu). Wij hebben het Zondag lunch menu genomen en het beviel ons zeer goed (prijs en kwealiteit). Ook was het erg gezellig met veel oudere Belgen waarmee we tijdens het genieten van de lunch binnen kwamen druppelen.