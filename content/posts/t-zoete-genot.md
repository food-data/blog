---
author: Onno Haldar
title: "'t Zoete Genot"
date: 2025-01-16
description: Lunch met Duite gerechten en Limburgse Vlaai
categories: [brasserie, eetcafé]
tags: [lunch]
---

['t Zoete Genot](https://zoetegenot.nl) in Arcen aan de Maas. Is gelegen aan het dorpsplein en biedt typische Limburgse gastvrijheid. Hier uitgebreid gelunched en als nagerecht van de chte heerlijke Limburgse Vlaai genoten. Meerdere kere daar geweest. De warme gerechten op het menu zijn veelal van Duitse afkomst (Schnitzel, Berlinner Worst, etc.).