---
author: Onno Haldar
title: City Hall leiden
date: 2024-07-02
description: Lunch in de Leidse binnenstad
categories: [brasserie, restaurant, italiaanse keuken]
tags: [lunch, anti-pasti, pizza]
---

[Restaurant City Hall](https://restaurantcityhall.nl) is gelegen in de binnenstad van Leiden (vlak achter het gemeentehuis). Het biedt een ruim terras waar je op gepaste afstand naar de voorbij-hangers kan kijken ;-). 

We hebben daar de *Pranzo brunchtafel* genomen. Dit is een zeer ruim assortiment met diverse antipasti, salade en pizza. De *Siciliaanse caponata* was met name zeer smakelijk.
