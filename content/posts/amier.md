---
author: Onno Haldar
title: Amier
date: 2024-05-24
description: Lunch met Libansese Tapas gerechten
categories: [restaurant, libanees]
tags: [lunch, tapas]
---

[Amier](https://amierrestaurant.nl) is een leuk familierestaurant met heerlijke Libansese keuken in de binnenstad van Den Haag. Omdat wij nog niet bekend waren met de Libansese keuken hadden we gekozen voor een Tapas met 4 gangen. Het is ons zeer goed bevallen want de gerechten waar zeer divers en heerlijk. Humus in diverse varianten met o.a. verse noten en granaatappel. Grituurde groenten, vis en vleesgerechten.
