---
author: Onno Haldar
title: Simonis aan Zee
date: 2024-06-21
description: Een lunch met visgerechten in Scheveningen
categories: [strandtent, bistro]
tags: [lunch, vis]
---

[Simonis aan Zee](https://www.beejtoen.nl) was ondanks de regenachtige dag een prima plek om van een heerlijke vislunch te genieten. We hebben de Vistrio en Ceasarsalade met Gamba's genomen. Smaakte prima en de koffie daarna was ook voor herhaling vatbaar.