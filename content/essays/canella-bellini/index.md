---
title: Cannela Bellini
date: 2024-06-01
tags: []
categories: []
weight: 50
show_comments: true
katex: false
draft: false
---

Dit gaat over een heerlijke samengestelde Italiaanse drank uit Venetie.

<!--more-->

De Canella cocktail “Bellini” is een Italiaanse klassieker van groot vakmanschap.

Bellini van Canella, de aperitivo uit Venetië, bestaat uit twee delen brut mousserende wijn, een deel sap, pulp van witte perziken en een paar druppels framboosextract. Dit zorgt voor de karakteristieke roze kleur geven. Canella voegt kort voor het bottelen de prosecco toe.

Canella gebruikt enkel witte perziken, rechtstreeks uit hun boomgaarden. De perziken worden pas geplukt als ze perfect rijp zijn.  Voor de maximale concentratie aan suikers en natuurlijke geuren.

Bellini van Canella bevat geen bewaarmiddelen of toegevoegde suikers, glutenvrij en caloriearm. Alcohol % = 5%.


Serveertip : drink Bellini goed gekoeld ! Perfect op een zomerse dag in de tuin of op het terras.

{{< figure src="./canella-bellini-fles.png" title="Fles Bellini van het merk Cannela" height="250" class="floatleft">}}

Te koop via winkel / webshops:
- [De Espeterhove - Barneveld](https://webshop.espeterhoeve.nl/bellini-canella-75-cl.html)
- [Boer Hoorn - Drenthe](https://www.hoorn-asperges.nl/product/bellini-20cl/)
- [Regina Paola - Belgie](https://reginapaola.be/shop/italiaanse-aperitivo/bellini-canella/)
