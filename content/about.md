---
title: "Over deze Site"
date: 2024-04-14
draft: false
---

## Lekker uit eten

We gaan vaak samen met gezin, familie vrienden en/of met elkaar uit eten. Meestal in de omgeving van Leiden, Den Haag en Delft. Soms tijdens onze vakanties en uitjes in Nederland, Belgie en verder.

Daarnaast ben ik zeer enthousiast over gezonde en lekker eten koken. Aanvankelijk aangestoken door bekende TV-koks zoals Jamie Oliver, Gorden Ramsey, Herman de Blijker etc. Daarna zelf mijn weg gezocht
