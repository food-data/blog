# Categorale indeling

Bron: [Soest Bestuurlijke Informatie](https://soest.bestuurlijkeinformatie.nl/Document/View/0a261241-2500-45ac-81ba-47055ddf4204&ved=2ahUKEwjAru2Q-reGAxW7wAIHHfT7Dm0QFnoECBEQAQ&usg=AOvVaw3N_gGDXEM4ZGmVr_gjcJb8)

## Categorie 1: Lichte horeca / dagzaken:

- broodjeszaak
- croissanterie
- koffiebar
- lunchroom
- ijssalon
- tearoom
- traiteur
- bezorg- of afhaalservice (bijv. pizza, chinees)

Hierbij gaat het om winkelondersteunende horeca zonder alcoholvergunning.

## Categorie 2: Horeca:

- hotel / motel / pension
- bistro
- brasserie
- restaurant
- eetcafé
- cafetaria / snackbar
- shoarma / grillroom / pizzeriazaken

Onder deze categorie vallen horeca-inrichtingen waarbij de exploitatie primair is gebaseerd op het
verstrekken van maaltijden / eten. Alleen hebben zij of een drank- & horecavergunning zoals bistro,
restaurant, eetcafé en/of zijn zij gedurende langere tijden geopend en hebben een
verkeersaantrekkende werking zoals shoarma- en pizzeriazaken. Ook bedrijven en inrichtingen gericht
op het verschaffen van logies, al of niet in combinatie met het verstrekken van consumpties en/of
dranken, vallen onder deze categorie.

## Categorie 3: Middelzware horeca:

- (grand)café
- bierhuis
- biljartcentrum/snookercafé
- proeflokaal

In deze categorie zitten de horecabedrijven waarbij de exploitatie primair is gebaseerd op het
verstrekken van dranken voor gebruik ter plaatse en die voor een goed functioneren ook een gedeelte
van de nacht geopend zijn. Daardoor kunnen zij aanmerkelijke hinder voor de omgeving met zich
meebrengen.

## Categorie 4: Zware horeca:

- zalenverhuur/ partycentrum (regulier gebruik t.b.v. feesten, muziek, dansevenementen)
- dancing
- discotheek
- bar / nachtclub
- casino of amusementscentrum, met horeca

Met de zware horeca worden de zaken aangeduid die gelet op vloeroppervlakte veel bezoekers trekken
en waar geluid een belangrijk onderdeel is van de bedrijfsvoering. Het casino is ook bij deze categorie
zware horeca geplaatst, gelet op de bijzondere wetgeving waarmee deze vorm van vrijetijdsbesteding
te maken heeft. Als zodanig veroorzaakt 